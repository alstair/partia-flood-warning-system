# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list, update_water_levels

station_list = build_station_list()
update_water_levels(station_list)


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_inconsistent_typical_range_stations1():
    """Check that the function can be called"""
    x = inconsistent_typical_range_stations(station_list)

def test_inconsistent_typical_range_stations2():
    """Check that the number of stations with inconsistent data is greater or equal than to zero and less than or equal to the total number of stations"""
    x = len(inconsistent_typical_range_stations(station_list))
    assert x >= 0
    assert x <= len(station_list)

def test_relative_water_level1():
    """Check that the method returns None if latest level data is not available"""
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s.latest_level = None

    assert s.relative_water_level() == None

def test_relative_water_level2():
    """Check that the method returns None if there is no typical level data available"""
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = None
    river = "River X"
    town = "My Town"
    
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s.latest_level = 0.8

    assert s.relative_water_level() == None

def test_relative_water_level3():
    """Check that the method returns None if the typical high - typical low is less than zero"""
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (8, 5)
    river = "River X"
    town = "My Town"
    
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s.latest_level = 0.8
    assert s.relative_water_level() == None

def test_relative_water_level4():
    """Check that the method returns None if the typical high - typical low equals zero"""
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0, 0)
    river = "River X"
    town = "My Town"
    
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s.latest_level = 0.8

    assert s.relative_water_level() == None

def test_relative_water_level5():
    """Check that the method returns a ratio of 1.0 if the latest water level corresponds to a level at the typical high"""
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (2.7, 5.2)
    river = "River X"
    town = "My Town"
    
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s.latest_level = 5.2

    assert s.relative_water_level() == 1.0
