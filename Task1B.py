from floodsystem.stationdata import build_station_list
from floodsystem.geo import haversine, stations_by_distance


def run():
    station_list = build_station_list()
    stations_by_distance_list = stations_by_distance(station_list, (52.2053, 0.1218))
    stations_and_town_by_distance = []
    for i in stations_by_distance_list:
        stations_and_town_by_distance.append((i[0].name, i[0].town, i[1]))
    print(stations_and_town_by_distance[:10])
    print()
    print(stations_and_town_by_distance[-10:])

run()