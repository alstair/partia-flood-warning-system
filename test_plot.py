from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from floodsystem.stationdata import build_station_list
import matplotlib.pyplot as plt
import datetime

stations_list = build_station_list()
dates =  [datetime.datetime(2020, 1, 10), datetime.datetime(2020, 1, 11), datetime.datetime(2020, 1, 12), datetime.datetime(2020, 1, 13), datetime.datetime(2020, 1, 14)]
levels = [1, 2, 3, 4, 5]

def test_plot_water_level():
    """Check that the function can be called"""
    plt.ion()
    x = plot_water_levels(stations_list[0], dates, levels)
    plt.close()
    

def test_plot_water_level_with_fit():
    """Check that the function can be called"""
    x = plot_water_level_with_fit(stations_list[0], dates, levels, 4)