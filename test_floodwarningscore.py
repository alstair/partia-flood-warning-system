from floodsystem.floodwarningscore import water_ratio_score, relative_ranking_score, geographical_proximity_score, stations_on_river_score, rising_level_score
from floodsystem.stationdata import build_station_list

stations_list = build_station_list()

#Limited tests for flood warning score functions as the vast majority of functions utilised are tested in other PyTest files.

def test_water_ratio_score1():
    """Check that the function can be called"""
    water_ratio_score(stations_list)

def test_relative_ranking_score1():
    """Check that the function can be called"""
    relative_ranking_score(stations_list)

def test_geographical_proximity_score1():
    """Check that the function can be called"""
    geographical_proximity_score(stations_list)

def test_stations_on_river_score1():
    """Check that the function can be called"""
    stations_on_river_score(stations_list)

def test_rising_level_score1():
    """Check that the function can be called"""
    rising_level_score(stations_list)