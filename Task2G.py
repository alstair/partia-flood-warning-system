"""Criteria:
    (COMPLETED) Water level ratio about certain values for each station. Severe (>5), High (5-1.5), Moderate (1.5-0), Low (<0)
    (COMPLETED) Ranking of station relative to other stations for water level ratio. Important if in top 100
    (COMPLETED) Gradient of past data points. Is water level rising or falling
    (COMPLETED) Consider other stations on the same river
    (COMPLETED) Geographical proximity to other stations with high water levels """

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.floodwarningscore import water_ratio_score, relative_ranking_score, geographical_proximity_score, stations_on_river_score, rising_level_score

stations = build_station_list()

def explanation():
    print("Flood warnings are issued based on a number of factors.")
    print("Depending on the severity level reached for a factor, a numerical value is added to a station total.")
    print("The total numerical value after all factors have been considered is used to issue the appropriate flood warning.")
    print("The greater the score the greater the risk of flooding.")
    print()

def run():
    update_water_levels(stations)
    for station in stations:
        station.flood_warning_score = 0
    water_ratio_score(stations)
    relative_ranking_score(stations)
    geographical_proximity_score(stations)
    stations_on_river_score(stations)
    rising_level_score(stations)

def severe():
    print("The following stations are at severe risk of flooding")
    print()
    for station in stations:
        if station.flood_warning_score >= 15:
            print(station.name, station.flood_warning_score)
    print()

def high():
    print("The following stations are at high risk of flooding")
    print()
    for station in stations:
        if station.flood_warning_score >= 10 and station.flood_warning_score <15:
            print(station.name, station.flood_warning_score)
    print()

def moderate():
    print("The following stations are at moderate risk of flooding")
    print()
    for station in stations:
        if station.flood_warning_score >= 5 and station.flood_warning_score <10:
            print(station.name, station.flood_warning_score)
    print()

def low():
    print("The following stations are at low risk of flooding")
    print()
    for station in stations:
        if station.flood_warning_score <5:
            print(station.name, station.flood_warning_score)
    print()

explanation()
run()
low()
moderate()
high()
severe()
