# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
from math import radians, cos, sin, asin, sqrt

def haversine(lat1, lon1, lat2, lon2):
    """Haversine formula to calculate the great circle distance
    between any two points"""
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371
    return c*r

def stations_by_distance(stations, p):
    station_distances = []
    for station in stations:
        distance = haversine(station.coord[0], station.coord[1], p[0], p[1])
        station_distances.append((station,distance))
    station_distances = sorted_by_key(station_distances,1)
    return station_distances

def stations_within_radius(stations, centre, r):
    stations_in_radius= []
    for station in stations:
        distance = haversine(station.coord[0], station.coord[1], centre[0], centre[1])
        if distance <= r:
            stations_in_radius.append(station)
    return stations_in_radius
           
def rivers_with_station(stations):
    rivers_with_station_list = []
    for station in stations:
        rivers_with_station_list.append(station.river)
    return set(rivers_with_station_list)

def stations_by_river(stations):
    stations_by_river_dictionary = {}
    for station in stations:
        stations_by_river_dictionary.setdefault(station.river, []).append(station.name)
    return stations_by_river_dictionary

def rivers_by_station_number(stations, N):
    rivers_by_station_number_list = []
    stations_by_river_dictionary = stations_by_river(stations)
    for river in stations_by_river_dictionary:
        rivers_by_station_number_list.append((river, len(stations_by_river_dictionary[river])))
    rivers_by_station_number_list = sorted(rivers_by_station_number_list, key=lambda river: river[1], reverse=True)
    if N > len(rivers_with_station(stations)) or N <= 0:
        raise ValueError("Input N is outide of range of rivers")
    elif isinstance(N, int) == False:
        raise ValueError("Input N is not an interger")
    else:
        n = N
        while (rivers_by_station_number_list[n][1] == rivers_by_station_number_list[N-1][1]) and (n < len(rivers_with_station(stations))):
            n += 1
        return rivers_by_station_number_list[0:n]