import datetime
import matplotlib.dates
import numpy as np

from .flood import stations_level_over_threshold, stations_highest_rel_level
from .geo import stations_within_radius, stations_by_river
from .datafetcher import fetch_measure_levels

def water_ratio_score(stations):
    #Checks the relative water level of each station and if it is above a certain level. 
    #Then adds the appropriate numerical value to the score.
    print("Analysing latest water level relative to typical range.")
    n = 0
    for station in stations:
        n += 1
        y = len(stations)
        if n == int(y/4):
            print("----- 25% COMPLETE -----")
        if n == int(2*y/4):
            print("----- 50% COMPLETE -----")
        if n == int(3*y/4):
            print("----- 75% COMPLETE -----")
        if station.relative_water_level() != None:
            if station.relative_water_level() >= 5:      
                station.flood_warning_score += 5
            elif station.relative_water_level() >= 1.5:
                station.flood_warning_score += 3
            elif station.relative_water_level() >=0:
                station.flood_warning_score += 1
    print("100% COMPLETE - Finished analysing latest water level relative to typical range.")
    print()

def relative_ranking_score(stations):
    #Lower score importance than relative water ratio as relative ranking is not as strong an indicator of flooding risk
    print("Analysing relative rank of stations based on latest relative water ratio.")
    x = stations_highest_rel_level(stations, 100)
    n = 0
    for station in stations:
        n += 1
        y = len(stations)
        if n == int(y/4):
            print("----- 25% COMPLETE -----")
        if n == int(2*y/4):
            print("----- 50% COMPLETE -----")
        if n == int(3*y/4):
            print("----- 75% COMPLETE -----")
        for i in x:
            if station.name == i[0]:
                station.flood_warning_score += 2
    print("100% COMPLETE - Finished analysing relative rank of stations based on latest relative water ratio.")
    print()

def geographical_proximity_score(stations):
    #Considers how many of the surrounding stations have significant relative water levels.
    #This helps to indicates flash floods that may eventually reach the station in questions.
    #Different to comparing stations on the same river as neighbouring rivers, brooks etc. may flood overland.
    print("Analysing water levels of other stations within a 5km radius of each station.")
    n = 0
    for station in stations:
        n += 1
        m = len(stations)
        if n == int(m/4):
            print("----- 25% COMPLETE -----")
        if n == int(2*m/4):
            print("----- 50% COMPLETE -----")
        if n == int(3*m/4):
            print("----- 75% COMPLETE -----")
        x = stations_within_radius(stations, station.coord, 5)
        y = len(x)
        z = 0
        for i in x:
            if i.relative_water_level() != None:
                if i.relative_water_level() >= 5:
                    z += 3
                elif i.relative_water_level() >= 3:
                    z += 2
                elif i.relative_water_level() >= 1:
                    z += 1
        geographical_proximity_ratio = z/y * 5
        if geographical_proximity_ratio > 5:
            station.flood_warning_score += 5
        elif geographical_proximity_ratio > 3:
            station.flood_warning_score += 3
        elif geographical_proximity_ratio > 1:
            station.flood_warning_score += 2
        elif geographical_proximity_ratio > 0:
            station.flood_warning_score += 1
    print("100% COMPLETE - Finished analysing water levels of other stations within a 5km radius of each station.")
    print()

def stations_on_river_score(stations):
    print("Analysing water levels of stations on the same river.")
    #Considers whether any stations on the same river have significant relative water levels.
    #This helps indicate fluvial floods (river floods)
    #Multiplied by a factor of five to allow clearer differentiation between values
    n = 0
    for station in stations:
        n += 1
        m = len(stations)
        if n == int(m/4):
            print("----- 25% COMPLETE -----")
        if n == int(2*m/4):
            print("----- 50% COMPLETE -----")
        if n == int(3*m/4):
            print("----- 75% COMPLETE -----")
        x = station.river
        z = 0
        for i in stations:
            if i.river == x:
                if i.relative_water_level() != None:
                    if i.relative_water_level() >= 5:
                        z += 3
                    elif i.relative_water_level() >= 3:
                        z += 2
                    elif i.relative_water_level() >= 1:
                        z += 1
        stations_on_river_ratio = z * 5
        if stations_on_river_ratio > 5:
            station.flood_warning_score += 5
        elif stations_on_river_ratio > 3:
            station.flood_warning_score += 3
        elif stations_on_river_ratio > 1:
            station.flood_warning_score += 2
        elif stations_on_river_ratio > 0:
            station.flood_warning_score += 1
    print("100% COMPLETE - Finished analysing water levels of stations on the same river.")     
    print()

def rising_level_score(stations):
    #Considers whether the water level at a station is rising or falling.
    #Collects water level for a station over a one day period and calculates the gradient of the line of best fit
    print("Analysing whether water level of stations are rising or falling.")
    dt = 1 
    n = 0
    for station in stations:
        try:
            dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
            x = matplotlib.dates.date2num(dates)
            water_level_change = 0
            n += 1
            y = len(stations)
            if n == int(y/10):
                print("----- 10% COMPLETE -----")
            if n == int(2*y/10):
                print("----- 20% COMPLETE -----")
            if n == int(3*y/10):
                print("----- 30% COMPLETE -----")
            if n == int(4*y/10):
                print("----- 40% COMPLETE -----")
            if n == int(5*y/10):
                print("----- 50% COMPLETE -----")
            if n == int(6*y/10):
                print("----- 60% COMPLETE -----")
            if n == int(7*y/10):
                print("----- 70% COMPLETE -----")
            if n == int(8*y/10):
                print("----- 80% COMPLETE -----")
            if n == int(9*y/10):
                print("----- 90% COMPLETE -----")
            if len(x) != 0:
                try:
                    p_coeff = np.polyfit(x - x[0], levels, 1)
                    water_level_change = p_coeff[0] * 10
                    #Can be used to check output of function
                    print(n, station.name, p_coeff[0])
                    if water_level_change >= 10:
                        station.flood_warning_score += 5
                    elif water_level_change >= 5:
                        station.flood_warning_score += 3
                    elif water_level_change >= 2:
                        station.flood_warning_score += 1
                    elif water_level_change > -2:
                        station.flood_warning_score += 0
                    elif water_level_change > -5:
                        station.flood_warning_score -= 1
                    elif water_level_change > 10:
                        station.flood_warning_score -= 3
                    else:
                        station.flood_warning_score -= 5
                #Used to ignore data if it possesses inf or NaN values
                except:
                    pass
        except:
            pass
    print("100% COMPLETE - Finished analysing whether water level of stations are rising or falling.")
    print()