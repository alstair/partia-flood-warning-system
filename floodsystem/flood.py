from .utils import sorted_by_key  # noqa 
from .station import inconsistent_typical_range_stations

def stations_level_over_threshold(stations, tol):
    list_of_stations_over_threshold = []
    # First use inconsistency function to remove stations with inconsistent data from list
    list_of_stations_with_inconsistent_data = inconsistent_typical_range_stations(stations)
    for station in list_of_stations_with_inconsistent_data:
        if station in stations:
            stations.remove(station)
    # As a redundancy, extra check for inconsistency added to remove stations with inconsistent data or unavailable current water level
    for station in stations:
        if station.relative_water_level() != None:
            if station.relative_water_level() >= tol:
                list_of_stations_over_threshold.append((station.name, station.relative_water_level()))
    list_of_stations_over_threshold = sorted_by_key(list_of_stations_over_threshold, 1, reverse=True)
    if len(list_of_stations_over_threshold) == 0:
        print("No stations have a relative water level greater than the inputed tolerance")
    return list_of_stations_over_threshold
        
def stations_highest_rel_level(stations, N):
    list_of_stations_by_relative_level = []
    for station in stations:
        if station.relative_water_level() != None:
            list_of_stations_by_relative_level.append((station.name, station.relative_water_level()))
    list_of_stations_by_relative_level = sorted_by_key(list_of_stations_by_relative_level, 1, reverse=True)
    if N > len(stations) or N <= 0:
        raise ValueError("Input N is outide of range of number of stations")
    else:
        return list_of_stations_by_relative_level[:N]