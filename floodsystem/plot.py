import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime, timedelta
from .analysis import polyfit


def plot_water_levels(station, dates, levels):
    plt.plot(dates, levels)

    plt.xlabel('date')
    plt.ylabel('water label (m)')
    plt.title(station.name)
    plt.xticks(rotation=45)
    plt.tight_layout()

    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):

    poly, d0 = polyfit(dates, levels, p)
    
    plt.plot(dates, levels, '.')
    plt.xlabel('date')
    plt.ylabel('water label (m)')
    plt.title(station.name)
    plt.xticks(rotation=45)
    plt.tight_layout()

    x1 = matplotlib.dates.date2num(dates)
    plt.plot(dates, poly(x1 - d0))
