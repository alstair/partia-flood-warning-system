from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station, stations_by_river

def run():
    station_list = build_station_list()
    rivers_with_station_list = rivers_with_station(station_list)
    print(len(rivers_with_station_list))
    sorted_list = sorted(rivers_with_station_list)
    print(sorted_list[:10])

    stations_by_river_dictionary = stations_by_river(station_list)
    print()
    print(sorted(stations_by_river_dictionary['River Aire']))
    print()
    print(sorted(stations_by_river_dictionary['River Cam']))
    print()
    print(sorted(stations_by_river_dictionary['River Thames']))

run()