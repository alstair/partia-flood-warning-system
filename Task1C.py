from floodsystem.stationdata import build_station_list
from floodsystem.geo import haversine, stations_within_radius

def run():
    station_list = build_station_list()
    stations_within_radius_list = stations_within_radius(station_list, (52.2053, 0.1218), 10)
    x = []
    for i in stations_within_radius_list:
        x.append(i.name)
    alphabetical_list = sorted(x)
    print(alphabetical_list)

run()