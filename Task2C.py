from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    station_list = build_station_list()
    update_water_levels(station_list)
    for stations in stations_highest_rel_level(station_list, 10):
        print(stations)

run()