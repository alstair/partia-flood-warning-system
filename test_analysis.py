from floodsystem.stationdata import build_station_list
from floodsystem.analysis import polyfit
import matplotlib
import datetime

stations_list = build_station_list()
dates =  [datetime.datetime(2020, 1, 10), datetime.datetime(2020, 1, 11), datetime.datetime(2020, 1, 12), datetime.datetime(2020, 1, 13), datetime.datetime(2020, 1, 14)]
levels = [1, 2, 3, 4, 5]

def test_polyfit_1():
    """Check that the function can be called"""
    x = polyfit(dates, levels, 4)

def test_polyfit_2():
    """Check that the length of the returned polynomial is of the order specified"""
    for i in range(1, 5):
        x = polyfit(dates, levels, i)
        assert len(x[0]) == i

def test_polyfit_3():
    """Check that the shift of the date time axis is sensible"""
    values = [1, 2, 3, 4]
    for i in values:
        assert polyfit(dates, levels, i)[1] == matplotlib.dates.date2num(dates[0])
        