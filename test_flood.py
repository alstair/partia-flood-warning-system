from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
import pytest

stations_list = build_station_list()
update_water_levels(stations_list)

def test_stations_level_over_threshold1():
    """Check that the function can be called"""
    x = stations_level_over_threshold(stations_list, 0)

def test_stations_level_over_threshold2():
    """Check that the number of stations returned is less than or equal to the total number of stations"""
    x = len(stations_list)
    y = len(stations_level_over_threshold(stations_list, -1000))
    assert x >= y

def test_stations_level_over_threshold3():
    """Check that the function only considers consistent typical low/high data"""
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (8, 5)
    river = "River X"
    town = "My Town"

    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    t = stations_list
    t.append(s)
    x = stations_level_over_threshold(t, 0)
    if s in x:
        raise ValueError

def test_stations_highest_rel_level1():
    """Checks that the function can be called"""
    x = stations_highest_rel_level(stations_list, 20)

def test_stations_highest_rel_level2():
    """Checks that the stations are in descending order by relative level"""
    x = stations_highest_rel_level(stations_list, 20)
    for i in range(len(x)-1):
        assert x[i][1] >= x[i+1][1]

def test_stations_highest_rel_level3():
    """Checks that the correct number of stations are printed"""
    x = stations_highest_rel_level(stations_list, 20)
    assert len(x) == 20

def test_stations_highest_rel_level4():
    """Checks that an error is raised when N <= 0"""    
    with pytest.raises(Exception):
        assert stations_highest_rel_level(stations_list, 0)

