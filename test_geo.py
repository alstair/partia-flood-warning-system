from floodsystem.geo import haversine, stations_by_distance, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number
from floodsystem.stationdata import build_station_list

stations_list = build_station_list()

def test_haversine0():
    """Check that the distance calculated when the same two locations are inputed is zero"""
    x = haversine(2,2,2,2)
    assert x == 0 

def test_haversine1():
    """Check that the distance between two known locations is correct using the haversine formula"""
    John_O_Groats = (58.6373, 3.0689)
    Lands_End = (50.0657, 5.7132)
    y = haversine(John_O_Groats[0], John_O_Groats[1], Lands_End[0], Lands_End[1])
    assert round(y,2) == 968.21

def test_stations_by_distance():
    """Test the building of the list of stations by distance"""
    list_stations_by_distance = stations_by_distance(stations_list, (0,0))
    assert len(list_stations_by_distance) > 0

def test_stations_within_radius1():
    """Test that stations are within range"""
    location = (52.2053, 0.1218)
    list_stations_within_radius = stations_within_radius(stations_list, location, 10)
    for i in list_stations_within_radius:
        distance = haversine(i.coord[0], i.coord[1], location[0], location[1])
        assert distance <= 10

def test_stations_within_radius2():
    #Cannot check for presence of individual stations as list of stations for which data is available may change over time
    """Test that when radius set to 0 no stations returned"""
    list_stations_within_radius = stations_within_radius(stations_list, (52.2053, 0.1218), 0)
    assert len(list_stations_within_radius) == 0

def test_stations_within_radius3():
    """Test that when radius set to 2000 stations returned equals total number of stations"""
    list_stations_within_radius = stations_within_radius(stations_list, (52.2053, 0.1218), 2000)
    assert len(stations_list) == len(list_stations_within_radius)

def test_rivers_with_station1():
    """Check that the function can be called"""
    x = rivers_with_station(stations_list)

def test_rivers_with_station2():
    """Test that the number of rivers with a station is greater than 0 and less than or equal to the total number of stations"""
    rivers_with_station_list = rivers_with_station(stations_list)
    assert len(rivers_with_station_list) > 0
    assert len(rivers_with_station_list) <= len(stations_list)

def test_stations_by_river1():
    """Check that the function can be called"""
    x = stations_by_river(stations_list)

def test_stations_by_river2():
    """Check that the number of keys equals the number of rivers with at least one monitoring station"""
    stations_by_river_dictionary = stations_by_river(stations_list)
    rivers_with_station_list = rivers_with_station(stations_list)
    assert len(stations_by_river_dictionary) == len(rivers_with_station_list) 

def test_rivers_by_station_number1():
    """Check that the function can be called"""
    x = rivers_by_station_number(stations_list, 1)

def test_rivers_by_station_number2():
    """Check a ValueError is raised for invalid values of N"""
    values = [len(rivers_with_station(stations_list)) + 1, -7, 2.3]
    for value in values:
        n = False
        try:
            x = rivers_by_station_number(stations_list, value)
        except ValueError:
            n = True
        assert n == True
