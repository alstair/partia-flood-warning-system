from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

def run():
    station_list = build_station_list()
    print()
    print(inconsistent_typical_range_stations(station_list))
    print()


run()