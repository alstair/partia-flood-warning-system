import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels

def run():
    station_list = build_station_list()
    update_water_levels(station_list)

    stations = []
    dt = 10

    for station in stations_highest_rel_level(station_list, 5):
        stations.append(station[0])

    for i in stations:
        for j in station_list:
            if i == j.name:
                dates, levels = fetch_measure_levels(j.measure_id, dt=datetime.timedelta(days=dt))
                plot_water_levels(j, dates, levels)
                

run()
