from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def run():
    station_list = build_station_list()
    print()
    print(rivers_by_station_number(station_list, 9))
    print()

run()